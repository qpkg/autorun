# Autorun for QNAP

Startup script for QTS

[![pipeline status](https://gitlab.com/qpkg/autorun/badges/master/pipeline.svg)](https://gitlab.com/qpkg/autorun/-/commits/master)

## Usage

Add files `*.sh` in folder `/etc/config/autorun.d/`

## Package Registry

https://gitlab.com/georglk-pkg/qpkg/-/packages


