#!/bin/sh
CONF=/etc/config/qpkg.conf
QPKG_NAME="autorun"
QPKG_ROOT=`/sbin/getcfg $QPKG_NAME Install_Path -f ${CONF}`
APACHE_ROOT=`/sbin/getcfg SHARE_DEF defWeb -d Qweb -f /etc/config/def_share.info`
export QNAP_QPKG=$QPKG_NAME

QPKG_CONFIG_PATH=$(/sbin/getcfg $QPKG_NAME Config_Path -f ${CONF})
QPKG_CONFIG_D_PATH="${QPKG_CONFIG_PATH}.d"
case "$1" in
  start)
    ENABLED=$(/sbin/getcfg $QPKG_NAME Enable -u -d FALSE -f $CONF)
    if [ "$ENABLED" != "TRUE" ]; then
        echo "$QPKG_NAME is disabled."
        exit 1
    fi
    : ADD START ACTIONS HERE
    if [ -d "$QPKG_CONFIG_D_PATH" ]
    then
      for FILE in $(ls "${QPKG_CONFIG_D_PATH}"/*.sh)
      do
        /bin/sh "${FILE}"
      done
    fi
    ;;

  stop)
    : ADD STOP ACTIONS HERE
    ;;

  restart)
    $0 stop
    $0 start
    ;;
  remove)
    ;;

  *)
    echo "Usage: $0 {start|stop|restart|remove}"
    exit 1
esac

exit 0
